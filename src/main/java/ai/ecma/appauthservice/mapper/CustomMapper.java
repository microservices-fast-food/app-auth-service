package ai.ecma.appauthservice.mapper;

import ai.ecma.appauthservice.payload.*;
import ai.ecma.appauthservice.utils.CommonUtils;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.*;

import java.util.*;

public class CustomMapper {
    public static RoleResDto roleToDto(Role role) {
        return new RoleResDto(
                role.getId(),
                role.getName(),
                role.getPermissions()
        );
    }

    public static Set<PermissionEnum> permissionToSet(PermissionEnum[] values) {
        return new HashSet<>(Arrays.asList(values));
    }


    public static AddressResDto addressToResDto(Address address) {
        return new AddressResDto(
                address.getId(),
                address.getLon(),
                address.getLat(),
                address.getFullAddress(),
                address.getDistrictId()
        );
    }

    public static NotificationResDTO notificationToResDTO(Notification notification) {
        return new NotificationResDTO(
                notification.getId(),
                notification.getTitle(),
                notification.getText(),
                notification.getPhotoId() != null ? notification.getPhotoId() : null,
                notification.getPhotoId() != null ? CommonUtils.buildPhotoUrl(notification.getPhotoId()) : null,
                notification.getSendTime(),
                notification.getReceiver()
        );
    }


    public static DistrictResDto districtToResDto(District district) {
        return new DistrictResDto(
                district.getId(),
                district.getName(),
                district.getRegionId()
        );
    }

    public static UserResDto userToResDto(User user) {
        return new UserResDto(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                districtToResDto(user.getDistrict()),
                user.getBirthdate(),
                user.getAvatarId() != null ? user.getAvatarId() : null,
                user.getAvatarId() != null ? CommonUtils.buildPhotoUrl(user.getAvatarId()) : null,
                roleToDto(user.getRole()),
                user.isEnabled(),
                user.getLanguage()
        );
    }

//    public static RoleResDto roleToDto(Role role) {
//        return new RoleResDto(
//                role.getId(),
//                role.getName(),
//                role.getPermissions()
//        );
//    }

    public static RegionResDto regionToResDto(Region region) {
        return new RegionResDto(
                region.getId(),
                region.getName()
        );
    }

    public static List<UserAddressResDto> userAddressListToDto(List<UserAddress> allByUserId) {
        List<UserAddressResDto> userAddressResDtoList = new ArrayList<>();
        for (UserAddress userAddress : allByUserId) {
            userAddressResDtoList.add(new UserAddressResDto(
                    userAddress.getId(),
                    userAddress.getUserId(),
                    userAddress.getAddressId(),
                    userAddress.getName()
            ));
        }
        return userAddressResDtoList;
    }

    public static UserAddressResDto userAddressToDto(UserAddress userAddress) {
        return new UserAddressResDto(
                userAddress.getId(),
                userAddress.getUserId(),
                userAddress.getAddressId(),
                userAddress.getName()
        );
    }
}
