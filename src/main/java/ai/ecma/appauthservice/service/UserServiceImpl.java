package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appauthservice.feign.ProductFeign;
import ai.ecma.appauthservice.mapper.CustomMapper;
import ai.ecma.appauthservice.mapper.UsersMapper;
import ai.ecma.appauthservice.payload.*;
import ai.ecma.appdblib.entity.enums.CourierStatusEnum;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.user.DistrictRepository;
import ai.ecma.appdblib.repository.user.RoleRepository;
import ai.ecma.appdblib.repository.user.UserRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final DistrictRepository districtRepository;
    private final UsersMapper usersMapper;
    private final ProductFeign productFeign;

    public UserServiceImpl(UserRepository userRepository, @Lazy PasswordEncoder passwordEncoder, RoleRepository roleRepository, DistrictRepository districtRepository, UsersMapper usersMapper, ProductFeign productFeign) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.districtRepository = districtRepository;
        this.usersMapper = usersMapper;
        this.productFeign = productFeign;
    }

    @PreAuthorize(value="hasAuthority('ADD_USER')")
    @Override
    public ApiResult<UserResDto> add(UserReqDto userReqDto) {
        boolean exists = userRepository.existsByPhoneNumber(userReqDto.getPhoneNumber());
        if (exists)
            throw new RestException(HttpStatus.BAD_REQUEST, "Bunday phone numberlik user allaqachon tizimda mavjud");

        UserPrincipal userPrincipal = new UserPrincipal(
                new User(
                        userReqDto.getFirstName(),
                        userReqDto.getLastName(),
                        userReqDto.getPhoneNumber(),
                        districtRepository.findById(userReqDto.getDistrictId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "District topilmadi")),
                        userReqDto.getBirthdate(),
                        userReqDto.getAvatarId() != null ? userReqDto.getAvatarId() : null,
                        roleRepository.findById(userReqDto.getRoleId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Role topilmadi")),
                        passwordEncoder.encode(userReqDto.getPassword()),
                        userReqDto.isEnabled(),
                        userReqDto.getLanguage()
                )
        );
        User user = userRepository.save(userPrincipal.getUser());
        return ApiResult.successResponse(CustomMapper.userToResDto(user));
    }

    @PreAuthorize(value="hasAuthority('VIEW_USER')")
    @Override
    public ApiResult<CustomPage<UserResDto>> getAllByPageable(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> userPage = userRepository.findAll(pageable);
        CustomPage<UserResDto> pageableUsers = userToCustomPage(userPage);
        return ApiResult.successResponse(pageableUsers);
    }

    @PreAuthorize(value="hasAuthority('VIEW_USER')")
    @Override
    public ApiResult<CustomPage<UserResDto>> getAllByRoleId(UUID roleId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> userPage = userRepository.findAllByRoleId(roleId, pageable);
        CustomPage<UserResDto> pageableUsersByRoleId = userToCustomPage(userPage);
        return ApiResult.successResponse(pageableUsersByRoleId);
    }

    @PreAuthorize(value="hasAuthority('VIEW_USER')")
    @Override
    public ApiResult<UserResDto> getOne(UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User topilmadi"));
        return ApiResult.successResponse(CustomMapper.userToResDto(user));
    }

    @PreAuthorize(value="hasAuthority('EDIT_USER')")
    @Override
    public ApiResult<UserResDto> editUser(UUID id, UserEditReqDto userEditReqDto) {
        boolean exists = userRepository.existsByPhoneNumberAndIdNot(userEditReqDto.getPhoneNumber() != null ? userEditReqDto.getPhoneNumber() : null, id);
        if (exists)
            throw new RestException(HttpStatus.BAD_REQUEST, "Bunday phone numberlik user allaqachon tizimda mavjud");

        User user = userRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User topilmadi"));

        user.setFirstName(userEditReqDto.getFirstName());
        user.setLastName(userEditReqDto.getLastName());
        user.setPhoneNumber(userEditReqDto.getPhoneNumber());
        user.setDistrict(districtRepository.findById(userEditReqDto.getDistrictId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "District topilmadi")));

        if (userEditReqDto.getAvatarId() != null) {
            productFeign.getById(userEditReqDto.getAvatarId());
            user.setAvatarId(userEditReqDto.getAvatarId());
        }

        user.setBirthdate(userEditReqDto.getBirthdate());
        user.setLanguage(userEditReqDto.getLanguage());

        userRepository.save(user);
        return ApiResult.successResponse(CustomMapper.userToResDto(user));
    }

    @PreAuthorize(value="hasAuthority('VIEW_USER')")
    @Override
    public ApiResult<List<UserDto>> getAllOnlineCouriersByBranch(UUID branchId) {
        List<User> couriers = userRepository.getAllOnlineCourierByBranchId(branchId, CourierStatusEnum.ONLINE);
        if (couriers.size()==0)
            throw new RestException(HttpStatus.NOT_FOUND,"Hozirda ushbu branch da online courier topilmadi");

        List<UserDto> userDtos = usersMapper.usersToUserDtoList(couriers);

        return ApiResult.successResponse(userDtos);
    }

    @Override
    public ApiResult<List<UserDto>> getUsersByIds(Set<UUID> ids) {
        List<User> users = userRepository.findAllByIdIn(ids);
        if (users.size() != ids.size())
            throw new RestException(HttpStatus.NOT_FOUND,"Ayrim userlar topilmadi");

        List<UserDto> userDtos = usersMapper.usersToUserDtoList(users);
        return ApiResult.successResponse(userDtos);
    }

    @PreAuthorize(value="hasAuthority('CHANGE_STATUS_USER')")
    @Override
    public ApiResult<?> changeStatusCourier(CourierStatusEnum status, User user) {
        user.setStatus(status);
        userRepository.save(user);
        return ApiResult.successResponse("OK");
    }

    @PreAuthorize(value="hasAuthority('ENABLE_USER')")
    @Override
    public ApiResult<UserResDto> enableUser(UUID id, boolean enable) {
        User user = userRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User topilmadi"));
        user.setEnabled(enable);
        userRepository.save(user);
        return ApiResult.successResponse(CustomMapper.userToResDto(user));
    }

    @PreAuthorize(value="hasAuthority('DELETE_USER')")
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            userRepository.deleteById(id);
            return ApiResult.successResponse("User muvaqffaqiyatli o'chirildi");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "User topilmadi");
        }
    }

    private CustomPage<UserResDto> userToCustomPage(Page<User> userPage) {
        return new CustomPage<>(
                userPage.getContent().stream().map(CustomMapper::userToResDto).collect(Collectors.toList()),
                userPage.getNumberOfElements(),
                userPage.getNumber(),
                userPage.getTotalElements(),
                userPage.getTotalPages(),
                userPage.getSize()
        );
    }
}
