package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.payload.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface AddressService {

    ApiResult<AddressResDto> add(AddressReqDto addressReqDto);

    ApiResult<CustomPage<AddressResDto>> getAll(int page, int size);

    ApiResult<AddressResDto> getOne(UUID id);

    ApiResult<AddressResDto> edit(AddressReqDto addressReqDto, UUID id);

    ApiResult<?> delete(UUID id);

    ApiResult<List<AddressForBranchDto>> getAddressesByIds(Set<UUID> ids);

    ApiResult<AddressForBranchDto> getOneAddress(UUID id);
}
