package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleReqDto {
    @NotBlank(message = "Name bo'sh bo'lmasligi kerak!")
    private String name;

    @NotEmpty(message = "Permission bo'sh bo'lmasligi kerak!")
    private Set<PermissionEnum> permissions;
}
