package ai.ecma.appauthservice.service;



import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.UserBranchReqDto;
import ai.ecma.appauthservice.payload.UserBranchResDto;

import java.util.UUID;

public interface UserBranchService {
    ApiResult<CustomPage<UserBranchResDto>> getAll(int page, int size);

    ApiResult<UserBranchResDto> getOne(UUID id);

    ApiResult<UserBranchResDto> add(UserBranchReqDto userBranchReqDto);

    ApiResult<UserBranchResDto> edit(UUID id, UserBranchReqDto userBranchReqDto);

    ApiResult<?> delete(UUID id);

}
