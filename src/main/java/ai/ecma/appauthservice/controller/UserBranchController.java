package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.UserBranchReqDto;
import ai.ecma.appauthservice.payload.UserBranchResDto;
import ai.ecma.appauthservice.utils.AppConstant;
import ai.ecma.appauthservice.utils.RestConstant;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.USER_BRANCH_CONTROLLER)
public interface UserBranchController {
    @GetMapping
    ApiResult<CustomPage<UserBranchResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                   @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("/{id}")
    ApiResult<UserBranchResDto> getOne(@PathVariable UUID id);

    @PostMapping
    ApiResult<UserBranchResDto> add(@RequestBody @Valid UserBranchReqDto userBranchReqDto);

    @PutMapping("/{id}")
    ApiResult<UserBranchResDto> edit(@PathVariable UUID id,
                                     @RequestBody @Valid UserBranchReqDto userBranchReqDto);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);

}
