package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.CourierStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private UUID id;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private CourierStatusEnum status;

}
