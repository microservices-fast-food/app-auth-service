package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.UserAddressReqDto;
import ai.ecma.appauthservice.payload.UserAddressResDto;
import ai.ecma.appauthservice.service.UserAddressService;
import ai.ecma.appdblib.payload.SpecialAddressResDto;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class UserAddressControllerImpl implements UserAddressController{
    private final UserAddressService userAddressService ;


    public UserAddressControllerImpl(UserAddressService userAddressService) {
        this.userAddressService = userAddressService;
    }


    @Override
    public ApiResult<UserAddressResDto> add(UserAddressReqDto userAddressReqDto, UserPrincipal userPrincipal) {
        return userAddressService.add(userAddressReqDto,userPrincipal);
    }

    @Override
    public ApiResult<List<SpecialAddressResDto>> getAllByUserId(UUID userId) {
        return userAddressService.getAllByUserId(userId);
    }

    @Override
    public ApiResult<UserAddressResDto> editName(UserAddressReqDto userAddressReqDto,
                                                 UserPrincipal userPrincipal) {
        return userAddressService.editName(userAddressReqDto, userPrincipal);
    }

    @Override
    public ApiResult<?> delete(UserAddressReqDto userAddressReqDto) {
        return userAddressService.deleteAddress(userAddressReqDto);
    }
}
