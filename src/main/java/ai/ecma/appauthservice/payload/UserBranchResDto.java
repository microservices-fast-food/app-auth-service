package ai.ecma.appauthservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBranchResDto {
    private UUID id;
    private UUID courierId;
    private UUID branchId;
    private boolean active;
}
