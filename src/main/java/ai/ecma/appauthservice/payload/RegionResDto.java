package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.RegionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegionResDto {
    private UUID id;
    private RegionEnum name;
}
