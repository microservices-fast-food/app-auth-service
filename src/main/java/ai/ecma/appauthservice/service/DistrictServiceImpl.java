package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appauthservice.mapper.CustomMapper;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.DistrictReqDto;
import ai.ecma.appauthservice.payload.DistrictResDto;
import ai.ecma.appdblib.entity.user.District;
import ai.ecma.appdblib.entity.user.Region;
import ai.ecma.appdblib.repository.user.DistrictRepository;
import ai.ecma.appdblib.repository.user.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DistrictServiceImpl implements DistrictService {
    private final DistrictRepository districtRepository;
    private final RegionRepository regionRepository;

    private District getDistrictById(UUID id) {
        return districtRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Bu tuman bazadan topilmadi"));
    }

    @PreAuthorize(value="hasAuthority('ADD_DISTRICT')")
    @Override
    public ApiResult<DistrictResDto> add(DistrictReqDto districtReqDto)  {
            boolean exists = districtRepository.existsByName(districtReqDto.getName());
            if (exists)
                throw new RestException(HttpStatus.BAD_REQUEST,"Bunday nomlik tuman bazada bor");
        Region region = regionRepository.findById(districtReqDto.getRegionId()).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, "Viloyat topilmadi"));
        District district=new District(
                districtReqDto.getName(),
                region.getId()
        );
        districtRepository.save(district);

            return ApiResult.successResponse(CustomMapper.districtToResDto(district));

    }

    @Override
    public ApiResult<CustomPage<DistrictResDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page,size, Sort.Direction.ASC,"region_id");
        Page<District> districtPage = districtRepository.findAll(pageable);
        CustomPage<DistrictResDto> districtResDtoCustomPage = districtToCustomPage(districtPage);
        return ApiResult.successResponse(districtResDtoCustomPage);
    }

    @Override
    public ApiResult<DistrictResDto> getOne(UUID id) {
        return ApiResult.successResponse(CustomMapper.districtToResDto(getDistrictById(id)));
    }

    @PreAuthorize(value="hasAuthority('EDIT_DISTRICT')")
    @Override
    public ApiResult<DistrictResDto> edit(DistrictReqDto districtReqDto, UUID id) {
        boolean exists = districtRepository.existsByNameAndIdNot(districtReqDto.getName(), id);
        if (exists)
            throw new RestException(HttpStatus.BAD_REQUEST,"BUnday nomli tuman mavjud");

        Region region = regionRepository.findById(districtReqDto.getRegionId()).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, "Viloyat topilmadi"));
        District district = districtRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, "Tuman topilmadi"));
        district.setName(districtReqDto.getName());
        district.setRegion(region);
        districtRepository.save(district);
        return ApiResult.successResponse(CustomMapper.districtToResDto(district));
    }

    @PreAuthorize(value = "hasAuthority('DELETE_DISTRICT')")
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            districtRepository.deleteById(id);
            return ApiResult.successResponse("Muvafaqiyatli o'chirildi");
        }catch (Exception e){
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Tumanni topilmadi");
        }
    }



    private CustomPage<DistrictResDto> districtToCustomPage(Page<District> districtPage) {
        return new CustomPage<>(
                districtPage.getContent().stream().map(CustomMapper::districtToResDto).collect(Collectors.toList()),
                districtPage.getNumberOfElements(),
                districtPage.getNumber(),
                districtPage.getTotalElements(),
                districtPage.getTotalPages(),
                districtPage.getSize()
        );
    }
}
