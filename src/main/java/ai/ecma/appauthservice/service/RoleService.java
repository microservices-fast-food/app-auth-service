package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.RoleAttachDto;
import ai.ecma.appauthservice.payload.RoleReqDto;
import ai.ecma.appauthservice.payload.RoleResDto;
import ai.ecma.appdblib.entity.enums.PermissionEnum;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface RoleService {
    ApiResult<List<RoleResDto>> getAll();

    ApiResult<Set<PermissionEnum>> getAllPermissions();

    ApiResult<RoleResDto> getOne(UUID id);

    ApiResult<RoleResDto> add(RoleReqDto roleReqDto);

    ApiResult<RoleResDto> edit(UUID id, RoleReqDto roleReqDto);

    ApiResult<?> delete(UUID id);

    ApiResult<?> attachRole(RoleAttachDto roleAttachDto);
}
