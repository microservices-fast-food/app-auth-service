package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appauthservice.mapper.UserBranchMapper;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.UserBranchReqDto;
import ai.ecma.appauthservice.payload.UserBranchResDto;
import ai.ecma.appdblib.entity.product.Branch;
import ai.ecma.appdblib.entity.product.UserBranch;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.product.BranchRepository;
import ai.ecma.appdblib.repository.product.UserBranchRepository;
import ai.ecma.appdblib.repository.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserBranchServiceImpl implements UserBranchService {
    private final UserBranchRepository userBranchRepository;
    private final UserBranchMapper userBranchMapper;
    private final BranchRepository branchRepository;
    private final UserRepository userRepository;

    @PreAuthorize(value="hasAuthority('VIEW_BRANCH_USER')")
    @Override
    public ApiResult<CustomPage<UserBranchResDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<UserBranch> userBranchPage = userBranchRepository.findAll(pageable);
        return ApiResult.successResponse(userBranchToCustomPage(userBranchPage));
    }

    @PreAuthorize(value="hasAuthority('VIEW_BRANCH_USER')")
    @Override
    public ApiResult<UserBranchResDto> getOne(UUID id) {
        UserBranch userBranch = userBranchRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User branch topilmadi"));
        return ApiResult.successResponse(userBranchMapper.userBranchToDto(userBranch));
    }

    @PreAuthorize(value="hasAuthority('ADD_BRANCH_USER')")
    @Override
    public ApiResult<UserBranchResDto> add(UserBranchReqDto userBranchReqDto) {
        User courier = userRepository.findById(userBranchReqDto.getCourierId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Courier topilmadi"));
        Branch branch = branchRepository.findById(userBranchReqDto.getBranchId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Filial topilmadi"));
        UserBranch userBranch = new UserBranch(
                courier,
                branch,
                userBranchReqDto.isActive()
        );
        userBranchRepository.save(userBranch);
        return ApiResult.successResponse(userBranchMapper.userBranchToDto(userBranch));
    }

    @PreAuthorize(value="hasAuthority('EDIT_BRANCH_USER')")
    @Override
    public ApiResult<UserBranchResDto> edit(UUID id, UserBranchReqDto userBranchReqDto) {
        UserBranch userBranch = userBranchRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User Branch topilmadi"));
        if (userBranch.getBranch().getId()!=userBranchReqDto.getBranchId()){
            Branch branch = branchRepository.findById(userBranchReqDto.getBranchId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Filial topilmadi"));
            userBranch.setBranch(branch);
        }
        userBranch.setActive(userBranchReqDto.isActive());
        userBranchRepository.save(userBranch);
        return ApiResult.successResponse(userBranchMapper.userBranchToDto(userBranch));
    }

    @PreAuthorize(value="hasAuthority('DELETE_BRANCH_USER')")
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            userBranchRepository.deleteById(id);
            return ApiResult.successResponse("O'chirildi");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "User Branch topilmadi");
        }
    }

    public CustomPage<UserBranchResDto> userBranchToCustomPage(Page<UserBranch> userBranchPage) {
        return new CustomPage<UserBranchResDto>(
                userBranchPage.getContent().stream().map(userBranchMapper::userBranchToDto).collect(Collectors.toList()),
                userBranchPage.getNumberOfElements(),
                userBranchPage.getNumber(),
                userBranchPage.getTotalElements(),
                userBranchPage.getTotalPages(),
                userBranchPage.getSize()
        );
    }
}
