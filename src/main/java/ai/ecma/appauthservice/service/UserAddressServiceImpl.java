package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appauthservice.mapper.CustomMapper;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.UserAddressReqDto;
import ai.ecma.appauthservice.payload.UserAddressResDto;
import ai.ecma.appdblib.entity.user.UserAddress;
import ai.ecma.appdblib.payload.SpecialAddressResDto;
import ai.ecma.appdblib.repository.user.AddressRepository;
import ai.ecma.appdblib.repository.user.UserAddressRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserAddressServiceImpl implements UserAddressService {
    private final UserAddressRepository userAddressRepository;
    private final AddressRepository addressRepository;

    public UserAddressServiceImpl(UserAddressRepository userAddressRepository, AddressRepository addressRepository) {
        this.userAddressRepository = userAddressRepository;
        this.addressRepository = addressRepository;
    }

    @Override
    public ApiResult<UserAddressResDto> add(UserAddressReqDto userAddressReqDto, UserPrincipal userPrincipal) {

        UserAddress userAddress = new UserAddress(
                userAddressReqDto.getName(),
                userPrincipal.getUser().getId(),
                userAddressReqDto.getAddressId()
        );
        userAddressRepository.save(userAddress);
        return ApiResult.successResponse(CustomMapper.userAddressToDto(userAddress));
    }

    @Override
    public ApiResult<List<SpecialAddressResDto>> getAllByUserId(UUID userId) {
        List<SpecialAddressResDto> allAddress = userAddressRepository.findAllByAddressByUserId(userId);
        return ApiResult.successResponse(allAddress);
    }

    @Override
    public ApiResult<UserAddressResDto> editName(UserAddressReqDto userAddressReqDto, UserPrincipal userPrincipal) {
        UserAddress userAddress = userAddressRepository.findByUserIdAndAddressId(userPrincipal.getUser().getId(), userAddressReqDto.getAddressId()).orElseThrow(() -> new
                RestException(HttpStatus.NOT_FOUND, "User yoki address topilmadi"));
        userAddress.setName(userAddressReqDto.getName());
        userAddressRepository.save(userAddress);
        return ApiResult.successResponse(CustomMapper.userAddressToDto(userAddress));
    }

    @Override
    public ApiResult<?> deleteAddress(UserAddressReqDto userAddressReqDto) {
        try {
            userAddressRepository.deleteByAddressId(userAddressReqDto.getAddressId());
            addressRepository.deleteById(userAddressReqDto.getAddressId());
            return ApiResult.successResponse("Address muvaffaqiyatli o'chirildi");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "User yoki address xato kiritildi");
        }
    }
}
