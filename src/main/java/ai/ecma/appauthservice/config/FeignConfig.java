package ai.ecma.appauthservice.config;

import feign.RequestInterceptor;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Muhammad Mo'minov
 * 23.09.2021
 */
@Configuration
public class FeignConfig {
    @Value("${service.authServiceUsername}")
    private String authServiceUsername;
    @Value("${service.authServicePassword}")
    private String authServicePassword;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header("serviceName", authServiceUsername);
            requestTemplate.header("servicePassword", authServicePassword);
            requestTemplate.header("Accept", ContentType.APPLICATION_JSON.getMimeType());
        };
    }
}
