package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appauthservice.mapper.CustomMapper;
import ai.ecma.appauthservice.mapper.NotificationMapper;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.NotificationReqDTO;
import ai.ecma.appauthservice.payload.NotificationResDTO;
import ai.ecma.appdblib.entity.user.Notification;
import ai.ecma.appdblib.repository.user.NotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;
    private final NotificationMapper notificationMapper;

    @PreAuthorize(value="hasAuthority('ADD_NOTIFICATION')")
    @Override
    public ApiResult<NotificationResDTO> add(NotificationReqDTO notificationReqDTO) {
        try {
            Notification notification = new Notification();

            //senTime dan boshqa hammasini reqdtodan entityga o'tkazadi sendTimeni o'zimiz logika bilan set qildik
            notificationMapper.updateNotification(notificationReqDTO,notification);
            notification.setSendTime(
                        notificationReqDTO.getSendTime() != null ? notificationReqDTO.getSendTime()
                                : new Timestamp(System.currentTimeMillis()));


            notificationRepository.save(notification);

            return ApiResult.successResponse(CustomMapper.notificationToResDTO(notification));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Rasm topilmadi");
        }
    }

    @Override
    public ApiResult<CustomPage<Notification>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Notification> notificationPage = notificationRepository.findAll(pageable);
        CustomPage<Notification> notificationCustomPage = notificationCustomPage(notificationPage);
        return ApiResult.successResponse(notificationCustomPage);
    }

    @Override
    public ApiResult<NotificationResDTO> getOne(UUID id) {
        Notification notification = notificationRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Notification topilmadi"));
        return ApiResult.successResponse(CustomMapper.notificationToResDTO(notification));
    }

    @PreAuthorize(value="hasAuthority('EDIT_NOTIFICATION')")
    @Override
    public ApiResult<NotificationResDTO> edit(UUID id, NotificationReqDTO notificationReqDTO) {
        try {
            Notification notification = notificationRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Notification topilmadi!"));
            notification.setTitle(notificationReqDTO.getTitle());
            notification.setText(notificationReqDTO.getText());
            notification.setSendTime(notificationReqDTO.getSendTime());
            notification.setReceiver(notificationReqDTO.getReceiver());
            notification.setPhotoId(notificationReqDTO.getPhotoId() != null ? notificationReqDTO.getPhotoId() : null);
            notificationRepository.save(notification);
            return ApiResult.successResponse(CustomMapper.notificationToResDTO(notification));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Rasm topilmadi!");
        }

    }

    @PreAuthorize(value="hasAuthority('DELETE_NOTIFICATION')")
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            notificationRepository.deleteById(id);
            return ApiResult.successResponse("Notification muvaffaqiyatli o'chirildi");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Notification topilmadi!");
        }
    }

    public static CustomPage<Notification> notificationCustomPage(Page<Notification> notificationPage) {
        return new CustomPage<>(
                notificationPage.getContent(),
                notificationPage.getNumberOfElements(),
                notificationPage.getNumber(),
                notificationPage.getTotalElements(),
                notificationPage.getTotalPages(),
                notificationPage.getSize()
        );
    }
}
