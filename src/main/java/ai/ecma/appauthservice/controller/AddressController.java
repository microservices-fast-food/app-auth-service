package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.*;
import ai.ecma.appauthservice.utils.RestConstant;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static ai.ecma.appauthservice.utils.AppConstant.DEFAULT_PAGE_NUMBER;
import static ai.ecma.appauthservice.utils.AppConstant.DEFAULT_PAGE_SIZE;

@RequestMapping(RestConstant.ADDRESS_CONTROLLER)
@Api(tags = "ADDRESS", description = "Manzil bo'yicha operatsiyalar")
public interface AddressController {

    @Operation(summary = "Yangi manzil qo'shish")
    @PostMapping
    ApiResult<AddressResDto> add(@RequestBody AddressReqDto addressReqDto);

    @Operation(summary = "Barcha manzillarni Ko'rish")
    @GetMapping
    ApiResult<CustomPage<AddressResDto>> getAll(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size);
    @Operation(summary = "Id orqali 1 ta manzilni ko'rish")
    @GetMapping("/{id}")
    ApiResult<AddressResDto> getOne(@PathVariable UUID id);

    @PutMapping("/{id}")
    ApiResult<AddressResDto> edit(@RequestBody @Valid AddressReqDto addressReqDto, @PathVariable UUID id);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);

    @PostMapping("/get-addresses-by-ids")
    ApiResult<List<AddressForBranchDto>> getAddressesByIds(@RequestBody Set<UUID> ids);

}
