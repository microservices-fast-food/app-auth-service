package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.ReceiverTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationResDTO {

    private UUID id;

    private String title;

    private String text;

    private UUID photoId;

    private String photoUrl;

    private Timestamp sendTime;

    private ReceiverTypeEnum receiver;
}
