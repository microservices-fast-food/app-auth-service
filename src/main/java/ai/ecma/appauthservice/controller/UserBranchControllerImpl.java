package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.UserBranchReqDto;
import ai.ecma.appauthservice.payload.UserBranchResDto;
import ai.ecma.appauthservice.service.UserBranchService;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class UserBranchControllerImpl implements UserBranchController{

    private final UserBranchService userBranchService;

    public UserBranchControllerImpl(UserBranchService userBranchService) {
        this.userBranchService = userBranchService;
    }

    @Override
    public ApiResult<CustomPage<UserBranchResDto>> getAll(int page, int size) {
        return userBranchService.getAll(page, size);
    }

    @Override
    public ApiResult<UserBranchResDto> getOne(UUID id) {
        return userBranchService.getOne(id);
    }

    @Override
    public ApiResult<UserBranchResDto> add(UserBranchReqDto userBranchReqDto) {
        return userBranchService.add(userBranchReqDto);
    }

    @Override
    public ApiResult<UserBranchResDto> edit(UUID id, UserBranchReqDto userBranchReqDto) {
        return userBranchService.edit(id, userBranchReqDto);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return userBranchService.delete(id);
    }

}
