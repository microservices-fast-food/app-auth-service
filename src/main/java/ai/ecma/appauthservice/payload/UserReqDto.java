package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.LanguageEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserReqDto {

    @NotBlank(message = "FirstName bo'sh bo'lmasligi kerak")
    private String firstName;

    @NotBlank(message = "LastName bo'sh bo'lmasligi kerak")
    private String lastName;

    @NotBlank(message = "Phone Number bo'sh bo'lmasligi kerak")
    @Pattern(regexp = "[+][9][9][8][0-9]{9}", message = "Telefon raqamining formatini xato kiritdingiz!")
    private String phoneNumber;

    @NotNull(message = "District bo'sh bo'lmasligi kerak")
    private UUID districtId;

    @NotNull(message = "Tug'ilgan kun bo'sh bo'lmasligi kerak")
    private Date birthdate;

    private UUID avatarId;

    @NotNull(message = "Role bo'sh bo'lmasligi kerak")
    private UUID roleId;

    @NotBlank(message = "Password bo'sh bo'lmasligi kerak")
    private String password;

    @NotNull(message = "Tizim uchun kirish huquqini berish bo'sh bo'lmasligi kerak")
    private boolean enabled;

    @NotNull(message = "Tizim tili bo'sh bo'lmasligi kerak")
    private LanguageEnum language;
}
