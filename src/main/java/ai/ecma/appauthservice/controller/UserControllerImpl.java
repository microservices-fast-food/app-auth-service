package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.*;
import ai.ecma.appauthservice.service.UserService;
import ai.ecma.appdblib.entity.enums.CourierStatusEnum;
import ai.ecma.appdblib.entity.user.User;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
public class UserControllerImpl implements UserController {
    private final UserService userService;

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ApiResult<UserResDto> add(UserReqDto userReqDto) {
        return userService.add(userReqDto);
    }

    @Override
    public ApiResult<CustomPage<UserResDto>> getAllByPageable(int page, int size) {
        return userService.getAllByPageable(page, size);
    }

    @Override
    public ApiResult<CustomPage<UserResDto>> getAllByRoleId(UUID roleId, int page, int size) {
        return userService.getAllByRoleId(roleId, page, size);
    }

    @Override
    public ApiResult<UserResDto> getOne(UUID id) {
        return userService.getOne(id);
    }

    @Override
    public ApiResult<UserResDto> editUser(UUID id, UserEditReqDto userEditReqDto) {
        return userService.editUser(id,userEditReqDto);
    }

    @Override
    public ApiResult<UserResDto> enableUser(UUID id, boolean enable) {
        return userService.enableUser(id, enable);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return userService.delete(id);
    }

    @Override
    public ApiResult<List<UserDto>> getAllOnlineCouriersByBranch(UUID branchId) {
        return userService.getAllOnlineCouriersByBranch(branchId);
    }

    @Override
    public ApiResult<?> changeStatusCourier(CourierStatusEnum status, User user) {

        return userService.changeStatusCourier(status, user);
    }

    @Override
    public ApiResult<List<UserDto>> getUsersByIds(Set<UUID> ids) {
        return userService.getUsersByIds( ids);
    }
}
