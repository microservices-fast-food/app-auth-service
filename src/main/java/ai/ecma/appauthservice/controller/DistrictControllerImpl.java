package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.DistrictReqDto;
import ai.ecma.appauthservice.payload.DistrictResDto;
import ai.ecma.appauthservice.service.DistrictService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class DistrictControllerImpl implements DistrictController {

    private final DistrictService districtService;

    @Override
    public ApiResult<DistrictResDto> add(DistrictReqDto districtReqDto) {
        return districtService.add(districtReqDto);
    }

    @Override
    public ApiResult<CustomPage<DistrictResDto>> getAll(int page, int size) {
        return districtService.getAll(page,size);
    }

    @Override
    public ApiResult<DistrictResDto> getOne(UUID id) {
        return districtService.getOne(id);
    }

    @Override
    public ApiResult<DistrictResDto> edit(@Valid DistrictReqDto districtReqDto, UUID id) {
        return districtService.edit(districtReqDto,id);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return districtService.delete(id);
    }
}
