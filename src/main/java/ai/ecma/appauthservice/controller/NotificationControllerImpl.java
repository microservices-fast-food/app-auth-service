package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.NotificationReqDTO;
import ai.ecma.appauthservice.payload.NotificationResDTO;
import ai.ecma.appauthservice.service.NotificationService;
import ai.ecma.appdblib.entity.user.Notification;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class NotificationControllerImpl implements NotificationController{
    private final NotificationService notificationService;

    public NotificationControllerImpl(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public ApiResult<NotificationResDTO> add(NotificationReqDTO notificationReqDTO) {
        return notificationService.add(notificationReqDTO);
    }

    @Override
    public ApiResult<CustomPage<Notification>> getAll(int page, int size) {
        return notificationService.getAll(page,size);
    }

    @Override
    public ApiResult<NotificationResDTO> getOne(UUID id) {
        return notificationService.getOne(id);
    }

    @Override
    public ApiResult<NotificationResDTO> edit(UUID id, NotificationReqDTO notificationReqDTO) {
        return notificationService.edit(id,notificationReqDTO);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return notificationService.delete(id);
    }
}
