package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.LanguageEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEditReqDto {

    @NotBlank(message = "Firstname bo'sh bo'lmasligi kerak")
    private String firstName;

    @NotBlank(message = "Lastname bo'sh bo'lmasligi kerak")
    private String lastName;

    @Pattern(regexp = "[+][9][9][8][0-9]{9}", message = "Telefon raqamining formatini xato kiritdingiz!")
    private String phoneNumber;

    @NotNull(message = "District bo'sh bo'lmasligi kerak")
    private UUID districtId;
    @NotNull(message = "Tug'ilgan kun bo'sh bo'lmasligi kerak")
    private Date birthdate;

    private UUID avatarId;
    @NotNull(message = "Tizim tili bo'sh bo'lmasligi kerak")
    private LanguageEnum language;
}
