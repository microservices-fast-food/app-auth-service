package ai.ecma.appauthservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressReqDto {

    @NotNull(message = "Lat qiymat bo'sh bo'lmasligi kerak")
    private Double lat;

    @NotNull(message = "Lon qiymat bo'sh bo'lmasligi kerak")
    private Double lon;

    @NotBlank(message = "To'liq addressi bo'sh bo'lmasligi kerak")
    private String fullAddress;

    @NotNull(message = "Tumani bo'sh bo'lmasligi kerak")  //agar bu yerda district id kelmay qolsa xato bo;ib qoladi
    private UUID districtId;
}
