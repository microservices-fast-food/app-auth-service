package ai.ecma.appauthservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChangePasswordReqDto {

    @NotNull(message = "User bo'sh bo'lmasligi kerak")
    private UUID userId;

    @NotBlank(message = "Password bo'sh bo'lmasligi kerak")
    private String oldPassword;

    @NotBlank(message = "Password bo'sh bo'lmasligi kerak")
    private String newPassword;

    @NotBlank(message = "Password bo'sh bo'lmasligi kerak")
    private String confirmationOfNewPassword;
}
