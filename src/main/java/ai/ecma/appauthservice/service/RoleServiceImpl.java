package ai.ecma.appauthservice.service;


import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appauthservice.mapper.CustomMapper;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.RoleAttachDto;
import ai.ecma.appauthservice.payload.RoleReqDto;
import ai.ecma.appauthservice.payload.RoleResDto;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.Role;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.user.RoleRepository;
import ai.ecma.appdblib.repository.user.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    public RoleServiceImpl(RoleRepository roleRepository, UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @PreAuthorize(value="hasAuthority('VIEW_ROLE')")
    @Override
    public ApiResult<List<RoleResDto>> getAll() {
        return ApiResult.successResponse(roleRepository.findAll().stream().map(CustomMapper::roleToDto).collect(Collectors.toList()));
    }

    @PreAuthorize(value="hasAuthority('VIEW_PERMISSION')")
    @Override
    public ApiResult<Set<PermissionEnum>> getAllPermissions() {
        PermissionEnum[] permissionEnums = PermissionEnum.values();
        return ApiResult.successResponse(CustomMapper.permissionToSet(permissionEnums));
    }

    @PreAuthorize(value="hasAuthority('VIEW_ROLE')")
    @Override
    public ApiResult<RoleResDto> getOne(UUID id) {
        Role role = roleRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Role topilmadi"));
        return ApiResult.successResponse(CustomMapper.roleToDto(role));
    }

    @PreAuthorize(value="hasAuthority('ADD_ROLE')")
    @Override
    public ApiResult<RoleResDto> add(RoleReqDto roleReqDto) {
        if (roleRepository.existsByName(roleReqDto.getName()))
            throw new RestException(HttpStatus.CONFLICT, "Bunday nomli rol tizimda mavjud");
        Role role = new Role(
                roleReqDto.getName(),
                roleReqDto.getPermissions()
        );
        roleRepository.save(role);
        return ApiResult.successResponse(CustomMapper.roleToDto(role));
    }

    @PreAuthorize(value="hasAuthority('EDIT_ROLE')")
    @Override
    public ApiResult<RoleResDto> edit(UUID id, RoleReqDto roleReqDto) {
        if (roleRepository.existsByIdNotAndName(id, roleReqDto.getName()))
            throw new RestException(HttpStatus.CONFLICT, "Bunday nomli rol tizimda mavjud");
        Role role = roleRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Bunday role topilmadi"));
        roleRepository.deleteOldPermissions(id);
        role.setName(roleReqDto.getName());
        role.setPermissions(roleReqDto.getPermissions());
        roleRepository.save(role);
        return ApiResult.successResponse(CustomMapper.roleToDto(role));
    }

    @PreAuthorize(value="hasAuthority('DELETE_ROLE')")
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            roleRepository.deleteById(id);
            return ApiResult.successResponse("Role o'chirildi");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Role topilmadi");
        }
    }

    @PreAuthorize(value="hasAuthority('ATTACH_ROLE')")
    @Override
    public ApiResult<?> attachRole(RoleAttachDto roleAttachDto) {
        Role role = roleRepository.findById(roleAttachDto.getRoleId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Role topilmadi"));
        List<User> users = userRepository.findAllById(roleAttachDto.getUserIds());
        if (users.size()!=roleAttachDto.getUserIds().size())
            throw new RestException(HttpStatus.CONFLICT,"Ma'lumot to'liq topilmadi");
        for (User user : users) {
            user.setRole(role);
        }
        userRepository.saveAll(users);
        return ApiResult.successResponse("Userlarga role biriktildi");
    }
}
