package ai.ecma.appauthservice.mapper;

import ai.ecma.appauthservice.payload.NotificationReqDTO;
import ai.ecma.appauthservice.payload.NotificationResDTO;
import ai.ecma.appauthservice.utils.CommonUtils;
import ai.ecma.appdblib.entity.user.Notification;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.UUID;

//klasni import qilib mapperda ishlatish uchun imports ishlatiladi
@Mapper(componentModel = "spring", imports = {UUID.class, CommonUtils.class})
public interface NotificationMapper {
    @Mapping(target = "photoUrl", expression = "java(CommonUtils.buildPhotoUrl(notification.getPhotoId()))")
    NotificationResDTO notificationToResDTO(Notification notification);

    @Mapping(target = "sendTime", ignore = true)
    void updateNotification(NotificationReqDTO notificationReqDTO, @MappingTarget Notification notification);
}
