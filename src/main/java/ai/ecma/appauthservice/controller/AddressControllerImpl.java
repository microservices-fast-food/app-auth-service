package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.*;
import ai.ecma.appauthservice.service.AddressService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AddressControllerImpl implements AddressController {

    private final AddressService addressService;

    @Override
    public ApiResult<AddressResDto> add(AddressReqDto addressReqDto) {
        log.info("AddressController add req addressReqDto:{}", addressReqDto);
        ApiResult<AddressResDto> apiResult = addressService.add(addressReqDto);
        log.info("AddressController add resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CustomPage<AddressResDto>> getAll(int page, int size) {
        log.info("AddressController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<AddressResDto>> apiResult = addressService.getAll(page, size);
        log.info("AddressController getAll resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<AddressResDto> getOne(UUID id) {
        log.info("AddressController getOne req id:{}",id);
        ApiResult<AddressResDto> apiResult = addressService.getOne(id);
        log.info("AddressController getOne resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<AddressResDto> edit(AddressReqDto addressReqDto, UUID id) {
        log.info("AddressController edit req addressReqDto:{}, id:{}", addressReqDto, id);
        ApiResult<AddressResDto> apiResult = addressService.edit(addressReqDto, id);
        log.info("AddressController edit resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        log.info("AddressController delete req id:{}", id);
        ApiResult<?> apiResult = addressService.delete(id);
        log.info("AddressController delete resp apiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<List<AddressForBranchDto>> getAddressesByIds(Set<UUID> ids) {
        log.info("AddressController getAddressesByIds req ids:{}", ids);
        ApiResult<List<AddressForBranchDto>> apiResult = addressService.getAddressesByIds(ids);
        log.info("AddressController getAddressesByIds resp apiResult:{}", apiResult);
        return apiResult;
    }

}
