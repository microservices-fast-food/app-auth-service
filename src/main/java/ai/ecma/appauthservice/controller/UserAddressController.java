package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.UserAddressReqDto;
import ai.ecma.appauthservice.payload.UserAddressResDto;
import ai.ecma.appauthservice.security.CurrentUser;
import ai.ecma.appauthservice.utils.RestConstant;
import ai.ecma.appdblib.payload.SpecialAddressResDto;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.USER_ADDRESS_CONTROLLER)
public interface UserAddressController {


    @PostMapping
    ApiResult<UserAddressResDto> add(@RequestBody UserAddressReqDto userAddressReqDto, @ApiIgnore @CurrentUser UserPrincipal userPrincipal);

    @GetMapping("/user-id/{userId}")
    ApiResult<List<SpecialAddressResDto>> getAllByUserId(@PathVariable UUID userId);

    @PutMapping("/edit-name")
    ApiResult<UserAddressResDto> editName(@RequestBody UserAddressReqDto userAddressReqDto,
                                          @ApiIgnore @CurrentUser UserPrincipal userPrincipal);

    @DeleteMapping("/delete-user")
    ApiResult<?> delete(@RequestBody UserAddressReqDto userAddressReqDto);

}
