package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.NotificationReqDTO;
import ai.ecma.appauthservice.payload.NotificationResDTO;
import ai.ecma.appauthservice.utils.AppConstant;
import ai.ecma.appauthservice.utils.RestConstant;
import ai.ecma.appdblib.entity.user.Notification;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.NOTIFICATION_CONTROLLER)
public interface NotificationController {

    @PostMapping
    ApiResult<NotificationResDTO> add(@RequestBody @Valid NotificationReqDTO notificationReqDTO);

    @GetMapping
    ApiResult<CustomPage<Notification>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                               @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("/{id}")
    ApiResult<NotificationResDTO> getOne(@PathVariable UUID id);

    @PutMapping("/{id}")
    ApiResult<NotificationResDTO> edit(@PathVariable UUID id,
                                       @Valid @RequestBody NotificationReqDTO notificationReqDTO);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
