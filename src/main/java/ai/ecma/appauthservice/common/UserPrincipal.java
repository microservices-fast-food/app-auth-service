package ai.ecma.appauthservice.common;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class UserPrincipal implements UserDetails {

    private final User user;
    private  Set<PermissionEnum> permissionEnums;

    public UserPrincipal(User user) {
        this.user = user;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        permissionEnums=user.getRole().getPermissions();
        return permissionEnums.stream().map(permissionEnum -> new SimpleGrantedAuthority(permissionEnum.name())).collect(Collectors.toSet());
    }

    public User getUser() {
        return user;
    }

    public Set<PermissionEnum> getPermissionEnums() {
        return permissionEnums;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getPhoneNumber();
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return user.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }
}
