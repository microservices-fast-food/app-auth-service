package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appauthservice.payload.*;
import ai.ecma.appauthservice.security.JWTProvider;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.enums.RoleEnum;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.entity.user.VerificationCode;
import ai.ecma.appdblib.repository.user.DistrictRepository;
import ai.ecma.appdblib.repository.user.RoleRepository;
import ai.ecma.appdblib.repository.user.UserRepository;
import ai.ecma.appdblib.repository.user.VerificationCodeRepository;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JWTProvider jwtProvider;
    private final VerificationCodeRepository verificationCodeRepository;
    private final TwilioService twilioService;
    private final RoleRepository roleRepository;
    private final DistrictRepository districtRepository;
    private final PasswordEncoder passwordEncoder;

    public AuthServiceImpl(UserRepository userRepository, @Lazy AuthenticationManager authenticationManager, JWTProvider jwtProvider, VerificationCodeRepository verificationCodeRepository, TwilioService twilioService, RoleRepository roleRepository, DistrictRepository districtRepository, @Lazy PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
        this.verificationCodeRepository = verificationCodeRepository;
        this.twilioService = twilioService;
        this.roleRepository = roleRepository;
        this.districtRepository = districtRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Value(value = "${verificationCodeLimit}")
    private int codeLimit;
    @Value(value = "${verificationCodeTime}")
    private long codeTimeLimit;
    @Value(value = "${verificationCodeExpiredTime}")
    private Long verificationCodeExpiredTime;


    @Override
    public ApiResult<TokenDto> signIn(LoginDto loginDto) {
        try {
            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginDto.getUsername(),
                    loginDto.getPassword()
            ));
            UserPrincipal userPrincipal = (UserPrincipal) authenticate.getPrincipal();
            String refreshToken = jwtProvider.generateTokenFromId(userPrincipal.getUser().getId(), false);
            String accessToken = jwtProvider.generateTokenFromId(userPrincipal.getUser().getId(), true);
            return ApiResult.successResponse(new TokenDto(accessToken, refreshToken));
        } catch (Exception e) {
            throw new RestException(HttpStatus.UNAUTHORIZED, "Password or username wrong!");
        }
    }

    @Override
    public ApiResult<?> checkPhoneNumber(PhoneNumberDto phoneNumberDto) {
        Timestamp pastTime = new Timestamp(System.currentTimeMillis() - codeTimeLimit);
        Long countSendingSmsCode = verificationCodeRepository.countAllByPhoneNumberAndCreatedAtAfter(phoneNumberDto.getPhoneNumber(), pastTime);
        if (countSendingSmsCode >= codeLimit)
            throw new RestException(HttpStatus.TOO_MANY_REQUESTS, "Ko`p urinish qildingiz, birozdan keyin urinib ko`ring!");
        String code = generateCode();
        boolean sendVerificationCode = twilioService.sendVerificationCode(code, phoneNumberDto.getPhoneNumber());
//        if (!sendVerificationCode)
//            throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, "Server xatoligi qayta urinib ko`ring!");
        VerificationCode verificationCode = new VerificationCode();
        verificationCode.setCode(code);
        verificationCode.setPhoneNumber(phoneNumberDto.getPhoneNumber());
        verificationCodeRepository.save(verificationCode);
        return ApiResult.successResponse("Sizning telefon raqamingizga sms code jo`natildi.");
    }

    @Override
    public ApiResult<RegisterDto> checkCode(CodeDto codeDto) {
        Timestamp pastTime = new Timestamp(System.currentTimeMillis() - verificationCodeExpiredTime);
        VerificationCode verificationCode = verificationCodeRepository.getByCondition(
                        codeDto.getPhoneNumber(), codeDto.getCode(), pastTime)
                .orElseThrow(() -> new RestException(HttpStatus.BAD_REQUEST, "Xato code Yubordiz"));
        Optional<User> optionalUser = userRepository.findByPhoneNumber(codeDto.getPhoneNumber());
        String accessToken = null;
        String refreshToken = null;
        boolean registered = false;
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            accessToken = jwtProvider.generateTokenFromId(user.getId(), true);
            refreshToken = jwtProvider.generateTokenFromId(user.getId(), false);
            registered = true;
        }
        verificationCode.setConfirmed(true);
        verificationCodeRepository.save(verificationCode);
        return ApiResult.successResponse(new RegisterDto(accessToken, refreshToken, registered));
    }

    @Override
    public ApiResult<TokenDto> signUp(SignUpDto signUpDto) {
        boolean confirmed = verificationCodeRepository.existsByPhoneNumberAndCodeAndConfirmedTrue(signUpDto.getPhoneNumber(), signUpDto.getCode());
        if (!confirmed)
            throw new RestException(HttpStatus.BAD_REQUEST, "SMS codeni xato kiritdingiz!");

        boolean exists = userRepository.existsByPhoneNumber(signUpDto.getPhoneNumber());
        if (exists)
            throw new RestException(HttpStatus.BAD_REQUEST, "Bu raqam oldin ro'yhatga olingan");

        UserPrincipal userPrincipal = new UserPrincipal(
                new User(
                        signUpDto.getFirstName(),
                        signUpDto.getLastName(),
                        signUpDto.getPhoneNumber(),
                        districtRepository.findById(signUpDto.getDistrictId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "District topilmadi")),
                        signUpDto.getBirthdate(),
                        signUpDto.getAttachmentId() != null
                                ? signUpDto.getAttachmentId()
                                : null,
                        roleRepository.findByName(RoleEnum.USER.name()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Role toplilmadi")),
                        passwordEncoder.encode(signUpDto.getPassword()),
                        true,
                        signUpDto.getLanguage()
                ));
        userRepository.save(userPrincipal.getUser());
        String accessToken = jwtProvider.generateTokenFromId(userPrincipal.getUser().getId(), true);
        String refreshToken = jwtProvider.generateTokenFromId(userPrincipal.getUser().getId(), false);
        return ApiResult.successResponse(new TokenDto(accessToken, refreshToken));
    }

    @Override
    public ApiResult<TokenDto> refreshToken(TokenDto tokenDto) {
        try {
            jwtProvider.validateToken(tokenDto.getAccessToken());
            return ApiResult.successResponse(tokenDto);
        } catch (ExpiredJwtException e) {
            try {
                jwtProvider.validateToken(tokenDto.getRefreshToken());
                UUID userId = UUID.fromString(jwtProvider.getIdFromToken(tokenDto.getRefreshToken()));
                return ApiResult.successResponse(new TokenDto(
                        jwtProvider.generateTokenFromId(userId, true),
                        jwtProvider.generateTokenFromId(userId, false)
                ));
            } catch (Exception ex) {
                throw new RestException(HttpStatus.UNAUTHORIZED, "Refresh token buzligan");
            }
        } catch (Exception e) {
            throw new RestException(HttpStatus.UNAUTHORIZED, "Access token buzligan");
        }
    }


    @Override
    public ApiResult<User> checkUser() {
        try {
            UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return ApiResult.successResponse(userPrincipal.getUser());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.UNAUTHORIZED, "Autorizatsiyadan o'tmagan");
        }
    }

    @Override
    public ApiResult<User> checkPermission(PermissionEnum externalPermission) {
        try {
            UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            for (PermissionEnum permissionEnum : userPrincipal.getPermissionEnums()) {
                if (permissionEnum.equals(externalPermission)) {
                    return ApiResult.successResponse(userPrincipal.getUser());
                }
            }
            throw new RestException(HttpStatus.FORBIDDEN, "Sizda bu yo'lga kirish uchun huquq yo'q");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.UNAUTHORIZED, "Autorizatsiyadan o'tmagan");
        }
    }

    @Override
    public ApiResult<?> changePassword(ChangePasswordReqDto changePasswordReqDto, UserPrincipal userPrincipal) {

        if (!changePasswordReqDto.getNewPassword().equals(changePasswordReqDto.getConfirmationOfNewPassword()))
            throw new RestException(HttpStatus.BAD_REQUEST, "Yangi parol tasdig'i noto'g'ri");

        User editing = userRepository.findById(userPrincipal.getUser().getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User topilmadi"));
        boolean matches = passwordEncoder.matches(changePasswordReqDto.getOldPassword(), userPrincipal.getPassword());
        if (!matches)
            throw new RestException(HttpStatus.BAD_REQUEST, "Eski parolingizni notog'ri kiritdingiz");

        editing.setPassword(passwordEncoder.encode(changePasswordReqDto.getNewPassword()));
        userRepository.save(editing);
        return ApiResult.successResponse("Password successfully changed");
    }

    @Override
    public ApiResult<TokenDto> forgotPassword(ForgetPasswordReqDto forgetPasswordReqDto) {
        Timestamp pastTime = new Timestamp(System.currentTimeMillis() - verificationCodeExpiredTime);
        VerificationCode verificationCode = verificationCodeRepository.getByCondition(
                        forgetPasswordReqDto.getPhoneNumber(), forgetPasswordReqDto.getCode(), pastTime)
                .orElseThrow(() -> new RestException(HttpStatus.BAD_REQUEST, "Xato code Yubordiz"));
        User user = userRepository.findByPhoneNumber(forgetPasswordReqDto.getPhoneNumber()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, " Tizimda Phone number topilmadi"));
        user.setPassword(passwordEncoder.encode(forgetPasswordReqDto.getNewPassword()));
        String accessToken = jwtProvider.generateTokenFromId(user.getId(), true);
        String refreshToken = jwtProvider.generateTokenFromId(user.getId(), false);
        verificationCode.setConfirmed(true);
        userRepository.save(user);
        verificationCodeRepository.save(verificationCode);
        return ApiResult.successResponse(new TokenDto(accessToken, refreshToken));
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByPhoneNumber(s).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Foydalanuvchi topilmadi"));
        return new UserPrincipal(user);
    }

    @Override
    public UserDetails loadById(UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User not found"));
        return new UserPrincipal(user);
    }

    public String generateCode() {
        String code = String.valueOf((int) (Math.random() * 10000000));
        code = code.substring(0, 6);
        System.out.println(code);
        return code;
    }

}
