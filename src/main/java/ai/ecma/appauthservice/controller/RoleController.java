package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.RoleAttachDto;
import ai.ecma.appauthservice.payload.RoleReqDto;
import ai.ecma.appauthservice.payload.RoleResDto;
import ai.ecma.appauthservice.utils.RestConstant;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RequestMapping(RestConstant.ROLE_CONTROLLER)
public interface RoleController {

    String ATTACH_ROLE = "/attach-role";

    @GetMapping
    ApiResult<List<RoleResDto>> getAll();

    @GetMapping("/permissions")
    ApiResult<Set<PermissionEnum>> getAllPermission();

    @GetMapping("/{id}")
    ApiResult<RoleResDto> getById(@PathVariable UUID id);

    @PostMapping
    ApiResult<RoleResDto> add(@RequestBody @Valid RoleReqDto roleReqDto);

    @PostMapping(ATTACH_ROLE)
    ApiResult<?> attachRole(@RequestBody @Valid RoleAttachDto roleAttachDto);

    @PutMapping("/{id}")
    ApiResult<RoleResDto> edit(@PathVariable UUID id, @RequestBody @Valid RoleReqDto roleReqDto);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);

}
