package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.NotificationReqDTO;
import ai.ecma.appauthservice.payload.NotificationResDTO;
import ai.ecma.appdblib.entity.user.Notification;

import java.util.UUID;

public interface NotificationService {
    ApiResult<NotificationResDTO> add(NotificationReqDTO notificationReqDTO);

    ApiResult<CustomPage<Notification>> getAll(int page, int size);

    ApiResult<NotificationResDTO> getOne(UUID id);

    ApiResult<NotificationResDTO> edit(UUID id, NotificationReqDTO notificationReqDTO);

    ApiResult<?> delete(UUID id);
}
