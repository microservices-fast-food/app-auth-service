package ai.ecma.appauthservice.mapper;

import ai.ecma.appauthservice.payload.UserBranchResDto;
import ai.ecma.appdblib.entity.product.UserBranch;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserBranchMapper {
    // source bu Entity, Target Payload resDto
    @Mapping(source = "userBranch.branch.id", target = "branchId")
    @Mapping(source = "userBranch.courier.id", target = "courierId")
    @Mapping(source = "id", target = "id") //
//    @Mapping(target = "id", ignore = true)
    UserBranchResDto userBranchToDto(UserBranch userBranch);
}
