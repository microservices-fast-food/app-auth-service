package ai.ecma.appauthservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBranchReqDto {


    @NotNull(message = "Filial bo'sh bo'lmasligi kerak")
    private UUID branchId;

    @NotNull(message = "Courier bo'sh bo'lmasligi kerak")
    private UUID courierId;

    @NotNull(message = "Aktivligi bo'sh bo'lmasligi kerak")
    private boolean active;
}
