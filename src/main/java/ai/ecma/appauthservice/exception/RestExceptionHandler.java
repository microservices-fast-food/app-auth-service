package ai.ecma.appauthservice.exception;

import ai.ecma.appauthservice.component.MessageByLang;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.ErrorData;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RestControllerAdvice
@RequiredArgsConstructor
public class RestExceptionHandler {
    private final MessageByLang messageByLang;


    @ExceptionHandler(value = RestException.class)
    public ResponseEntity<?> exceptionHandling(RestException restException) {
        restException.printStackTrace();
        return ResponseEntity.
                status(restException.getStatus())
                .body(ApiResult.errorResponse(restException.getMessage(),restException.getObject()));
    }


    @ExceptionHandler(value = TokenExpiredException.class)
    public ResponseEntity<?> exceptionHandling() {
        return ResponseEntity.
                status(498)
                .body(ApiResult.errorResponse("Token expired"));
    }


    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<?> exceptionHandling(MethodArgumentNotValidException e){
        List<ErrorData> errorData = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(objectError -> errorData.add(
                new ErrorData(objectError.getDefaultMessage(), 400)
                )
        );
        return ResponseEntity.status(400).body(ApiResult.errorResponse(errorData));
    }


    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> exceptionHandling(Exception e){
        e.printStackTrace();
        return ResponseEntity
                .status(500)
                .body(ApiResult.errorResponse("Internal server error"));
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<?> exceptionHandling(HttpMessageNotReadableException ex) {
        ex.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ApiResult.errorResponse("Parametr xato tipda berildi!"));
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<?> exceptionHandling(AccessDeniedException ex) {
        ex.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(ApiResult.errorResponse("Bu yo'lga kirishga sizda huquq yo'q"));
    }


    /**
     * BU FOREIGN KEY LARNI TOPOLMAGANDA BERUVCHI EXCEPTION UNI ICHIDAN
     * COLUMN NAME NI OLIB CLIENTGA QAYTARADI
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    public ResponseEntity<?> handleException(DataIntegrityViolationException ex) {
        ex.printStackTrace();
        try {
            SQLException sqlException = ((ConstraintViolationException) ex.getCause()).getSQLException();
            String message = sqlException.getMessage();
            String detail = "Detail:";

            //DETAIL: SO'ZINI INDEKSINI ANIQLAB OLYAPMAN ARENTIR SIFATIDA
            int arentir = message.indexOf(detail);

            //DETAIL SO'ZIDAN KEYINGI OCHILGAN 1-QAVS NI INDEXINI OLIB +1 QO'SHTIM
            int fromColumnName = message.indexOf("(", arentir) + 1;

            //DETAIL SO'ZIDAN KEYINGI YOPILGAN 1-QAVS NI INDEXINI OLIB -3 AYIRDIM
            int toColumnName = message.indexOf(")", fromColumnName) - 3;

            //MESSAGEDAN COLUMN NAME NI QIRQIB OLIB UNI UPPER_CASE QILINDI
            String columnName = message.substring(fromColumnName, toColumnName).toUpperCase(Locale.ROOT);

            //MESSAGE_BY_LANG GA BERISH UCHUN
            String clientMessage = columnName + "_NOT_FOUND";
            return new ResponseEntity<>(
                    ApiResult.errorResponse(messageByLang.getMessageByKey(clientMessage)),
                    HttpStatus.NOT_FOUND
            );
        } catch (Exception exception) {
            exception.printStackTrace();
            return new ResponseEntity<>(
                    ApiResult.errorResponse("Server error. Please try again"),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
