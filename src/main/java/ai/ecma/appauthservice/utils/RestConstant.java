package ai.ecma.appauthservice.utils;

public interface RestConstant {

    String BASE_PATH = "/api/open-auth";
    String DOMAIN = "http://localhost"; //www.fast-food.uz

    String AUTH_CONTROLLER = BASE_PATH + "/auth";
    String ROLE_CONTROLLER = BASE_PATH + "/role";
    String ADDRESS_CONTROLLER = BASE_PATH + "/address";
    String DISTRICT_CONTROLLER = BASE_PATH + "/district";
    String NOTIFICATION_CONTROLLER = BASE_PATH + "/notification";
    String USER_CONTROLLER = BASE_PATH + "/user";
    String USER_ADDRESS_CONTROLLER = BASE_PATH + "/user-address";
    String USER_BRANCH_CONTROLLER = BASE_PATH + "/user-branch";


    //PRODUCT SERVICE NING ASOSIY YO'LI BU
    String PRODUCT_SERVICE = "PRODUCT-SERVICE/api/open-product";

    String ATTACHMENT_CONTROLLER = "/api/open-product/attachment";

    String REQUEST_ATTRIBUTE_CURRENT_USER = "user";
}
