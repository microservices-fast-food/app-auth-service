package ai.ecma.appauthservice.component;

import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appdblib.entity.enums.LanguageEnum;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.enums.RegionEnum;
import ai.ecma.appdblib.entity.enums.RoleEnum;
import ai.ecma.appdblib.entity.user.District;
import ai.ecma.appdblib.entity.user.Region;
import ai.ecma.appdblib.entity.user.Role;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.product.ProductRepository;
import ai.ecma.appdblib.repository.user.DistrictRepository;
import ai.ecma.appdblib.repository.user.RegionRepository;
import ai.ecma.appdblib.repository.user.RoleRepository;
import ai.ecma.appdblib.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final RegionRepository regionRepository;
    private final DistrictRepository districtRepository;

    public DataLoader(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, RegionRepository regionRepository, DistrictRepository districtRepository, ProductRepository productRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.regionRepository = regionRepository;
        this.districtRepository = districtRepository;
    }


    @Value("${spring.sql.init.mode}")
    private String dataLoaderMode;

    @Override
    public void run(String... args) throws Exception {
        roleRepository.uniqueCreator();

        if (dataLoaderMode.equals("always")) {

            //regionEnumlarni aylanib regionlistga regionlarni qo'shib chiqildi va regionlist db saqlandi
            RegionEnum[] regionEnums = RegionEnum.values();
            List<Region> regions = new ArrayList<>();
            for (RegionEnum regionEnum : regionEnums) {
                regions.add(new Region(regionEnum));
            }
            regionRepository.saveAll(regions);

            //oldindan bir regioni topib olib , uni chaqirib shu uchun districtlar yaratayotganda region id berib ketishlik uchun
            Region region = regionRepository.findByName(RegionEnum.TOSHKENT_SH).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Region name topilmadi!"));

            //toshkent sh uchun tumanlar nomlari oldindan qo'shildi
            List<String> districtsName = new ArrayList<>(Arrays.asList(
                    "Yakkasaroy tumani",
                    "Yunusobod tumani",
                    "Shayxontohur tumani",
                    "Chilonzor tumani",
                    "Yashnobod tumani",
                    "Uchtepa tumani",
                    "Sirg'ali tumani",
                    "Mirobod tumani",
                    "Mirzo Ulug'bek tumani",
                    "Bektimer tumani",
                    "Olmazor tumani"
            ));

            //districtlarga name va yuqorida nomi orqali chaqirib olingan region id berib chiqildi va saqlandi
            List<District> districtList = new ArrayList<>();
            for (String name : districtsName) {
                districtList.add(new District(
                        name,
                        region.getId()
                ));
            }
            districtRepository.saveAll(districtList);
            System.out.println(districtList.get(0).getId());


            Role adminRole = roleRepository.save(new Role(
                    RoleEnum.ADMIN.name(),
                    new HashSet<>(Arrays.asList(PermissionEnum.values()))
            ));

            Role userRole = roleRepository.save(new Role(
                    RoleEnum.USER.name(),
                    new HashSet<>(RoleEnum.USER.getPermissionEnums())

            ));

            Role courierRole = roleRepository.save(new Role(
                    RoleEnum.COURIER.name(),
                    new HashSet<>(RoleEnum.COURIER.getPermissionEnums())

            ));

            Role operatorRole = roleRepository.save(new Role(
                    RoleEnum.OPERATOR.name(),
                    new HashSet<>(RoleEnum.OPERATOR.getPermissionEnums())

            ));

            Role dispatcherRole = roleRepository.save(new Role(
                    RoleEnum.DISPATCHER.name(),
                    new HashSet<>(RoleEnum.DISPATCHER.getPermissionEnums())

            ));

            Role kitchenRole = roleRepository.save(new Role(
                    RoleEnum.KITCHEN.name(),
                    new HashSet<>(RoleEnum.KITCHEN.getPermissionEnums())

            ));

            Role paymeRole = roleRepository.save(new Role(
                    RoleEnum.PAYME.name(),
                    new HashSet<>(RoleEnum.PAYME.getPermissionEnums())

            ));


            List<User> userList = new ArrayList<>(Arrays.asList(
                    new User(
                            "admin",
                            "admin",
                            "+998971234567",
                            districtList.get(0),
                            Date.valueOf("2021-01-01"),
                            null,
                            adminRole,
                            passwordEncoder.encode("admin"),
                            true,
                            LanguageEnum.UZ
                    ),
                    new User(
                            "user",
                            "user",
                            "+998971234568",
                            districtList.get(0),
                            Date.valueOf("2021-01-01"),
                            null,
                            userRole,
                            passwordEncoder.encode("user"),
                            true,
                            LanguageEnum.UZ
                    ),
                    new User(
                            "courier",
                            "courier",
                            "+998971234569",
                            districtList.get(0),
                            Date.valueOf("2021-01-01"),
                            null,
                            courierRole,
                            passwordEncoder.encode("courier"),
                            true,
                            LanguageEnum.UZ
                    ),
                    new User(
                            "operator",
                            "operator",
                            "+998971234560",
                            districtList.get(0),
                            Date.valueOf("2021-01-01"),
                            null,
                            operatorRole,
                            passwordEncoder.encode("operator"),
                            true,
                            LanguageEnum.UZ
                    ),
                    new User(
                            "dispatcher",
                            "dispatcher",
                            "+998971234561",
                            districtList.get(0),
                            Date.valueOf("2021-01-01"),
                            null,
                            dispatcherRole,
                            passwordEncoder.encode("dispatcher"),
                            true,
                            LanguageEnum.UZ
                    ),
                    new User(
                            "kitchen",
                            "kitchen",
                            "+998971234562",
                            districtList.get(0),
                            Date.valueOf("2021-01-01"),
                            null,
                            kitchenRole,
                            passwordEncoder.encode("kitchen"),
                            true,
                            LanguageEnum.UZ
                    ),
                    new User(
                            "payme",
                            "payme",
                            "+998971234563",
                            districtList.get(0),
                            Date.valueOf("2021-01-01"),
                            null,
                            paymeRole,
                            passwordEncoder.encode("payme"),
                            true,
                            LanguageEnum.UZ
                    )
            ));

            userRepository.saveAll(userList);
            System.out.println(userList);
        }
    }
}
