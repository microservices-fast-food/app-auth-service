package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.payload.*;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.UUID;

public interface AuthService extends UserDetailsService {

    UserDetails loadById(UUID id);

    ApiResult<TokenDto> signIn(LoginDto loginDto);

    ApiResult<?> checkPhoneNumber(PhoneNumberDto phoneNumberDto);

    ApiResult<RegisterDto> checkCode(CodeDto codeDto);

    ApiResult<TokenDto> signUp(SignUpDto signUpDto);

    ApiResult<TokenDto> refreshToken(TokenDto tokenDto);

    ApiResult<User> checkUser();

    ApiResult<User> checkPermission(PermissionEnum permissionEnum);

    ApiResult<?> changePassword(ChangePasswordReqDto changePasswordReqDto, UserPrincipal userPrincipal);

    ApiResult<TokenDto> forgotPassword(ForgetPasswordReqDto forgetPasswordReqDto);
}
