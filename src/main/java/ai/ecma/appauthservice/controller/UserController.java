package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.*;
import ai.ecma.appauthservice.security.CurrentUser;
import ai.ecma.appauthservice.utils.RestConstant;
import ai.ecma.appdblib.entity.enums.CourierStatusEnum;
import ai.ecma.appdblib.entity.user.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static ai.ecma.appauthservice.utils.AppConstant.DEFAULT_PAGE_NUMBER;
import static ai.ecma.appauthservice.utils.AppConstant.DEFAULT_PAGE_SIZE;

@RequestMapping(RestConstant.USER_CONTROLLER)
public interface UserController {

    /**
     * Admin tomonidan userni qo'shish
     *
     * @param userReqDto (barcha kerakli malumotlar)
     * @return UserResDto (saqlagan userini kerakli fieldlarini qaytarish)
     */
    @PostMapping
    ApiResult<UserResDto> add(@RequestBody @Valid UserReqDto userReqDto);


    /**
     * Userlarni hammasini sahifalab olish
     *
     * @param page (0)
     * @param size (20)
     * @return CustomePage<UserResDto>>
     */
    @GetMapping
    ApiResult<CustomPage<UserResDto>> getAllByPageable(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
                                                       @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size);


    /**
     * Role orqali userlarni hammasini sahifalab olish
     *
     * @param roleId (uuid)
     * @param page   (0)
     * @param size   (20)
     * @return CustomPage<UserResDto>>
     */
    @GetMapping("/role-id/{roleId}")
    ApiResult<CustomPage<UserResDto>> getAllByRoleId(@PathVariable UUID roleId,
                                                     @RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
                                                     @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size);

    /**
     * Bitta userni olish
     *
     * @param id (uuid)
     * @return UserResDto
     */
    @GetMapping("/{id}")
    ApiResult<UserResDto> getOne(@PathVariable UUID id);


    /**
     * Userning o'zi uchun o'zini tahrirlash
     *
     * @param id             (uuid)
     * @param userEditReqDto (userEditReqDto)
     * @return UserResDto
     */
    @PutMapping("/{id}")
    ApiResult<UserResDto> editUser(@PathVariable UUID id,
                                   @RequestBody UserEditReqDto userEditReqDto);

    /**
     * Userning tizimga kira olish huquqini cheklab qo'yish
     *
     * @param id     (uuid)
     * @param enable (boolean)
     * @return UserResDto
     */
    @PostMapping("/enable/{id}")
    ApiResult<UserResDto> enableUser(@PathVariable UUID id,
                                     @RequestParam boolean enable);


    /**
     * Userni o'chirish
     *
     * @param id (uuid)
     * @return "Muvaffaqiyatli o'chirildi degan xabar"
     */
    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);


    /**
     * biz branchid orqali hozirda shu branch da buyurtma olish uchun online bo'lgan
     * shu branchga tegishli courier larni qaytaramiz
     * @param branchId
     * @return
     */
    @GetMapping("/get-online-couriers-by-branch/{branchId}")
    ApiResult<List<UserDto>> getAllOnlineCouriersByBranch(@PathVariable UUID branchId);

    /**
     * Courier ni statusini o'zgartirrish uchun
     * @param status
     * @param user
     * @return
     */
    @PutMapping("/change-status-courier")
    ApiResult<?> changeStatusCourier(@RequestBody CourierStatusEnum status, @CurrentUser User user);

    /**
     * user id lar orqali shu idlarni address listini olish
     *
     */
    @GetMapping("/get-users-by-ids")
    ApiResult<List<UserDto>>  getUsersByIds(@RequestBody Set<UUID> ids);
}
