package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.RoleAttachDto;
import ai.ecma.appauthservice.payload.RoleReqDto;
import ai.ecma.appauthservice.payload.RoleResDto;
import ai.ecma.appauthservice.service.RoleService;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
public class RoleControllerImpl implements RoleController{
    private final RoleService roleService;

    public RoleControllerImpl(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public ApiResult<List<RoleResDto>> getAll() {
        return roleService.getAll();
    }

    @Override
    public ApiResult<Set<PermissionEnum>> getAllPermission() {
        return roleService.getAllPermissions();
    }

    @Override
    public ApiResult<RoleResDto> getById(UUID id) {
        return roleService.getOne(id);
    }

    @Override
    public ApiResult<RoleResDto> add(RoleReqDto roleReqDto) {
        return roleService.add(roleReqDto);
    }

    @Override
    public ApiResult<?> attachRole(RoleAttachDto roleAttachDto) {
        return roleService.attachRole(roleAttachDto);
    }

    @Override
    public ApiResult<RoleResDto> edit(UUID id, RoleReqDto roleReqDto) {
        return roleService.edit(id,roleReqDto);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return roleService.delete(id);
    }
}
