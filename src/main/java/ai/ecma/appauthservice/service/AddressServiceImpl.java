package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.exception.RestException;
import ai.ecma.appauthservice.mapper.AddressMapper;
import ai.ecma.appauthservice.mapper.CustomMapper;
import ai.ecma.appauthservice.payload.*;
import ai.ecma.appdblib.entity.user.Address;
import ai.ecma.appdblib.repository.user.AddressRepository;
import ai.ecma.appdblib.repository.user.UserAddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final UserAddressRepository userAddressRepository;
    private final AddressMapper addressMapper;


    private Address getAddressById(UUID id) {
        return addressRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Bu address bazadan topilmadi"));
    }

    @Override
    public ApiResult<AddressResDto> add(AddressReqDto addressReqDto) {
        try {
            Address address = addressRepository.save(
                    new Address(
                            addressReqDto.getLat(),
                            addressReqDto.getLon(),
                            addressReqDto.getFullAddress(),
                            addressReqDto.getDistrictId() //tekshirish kerak
                    )
            );
            return ApiResult.successResponse(CustomMapper.addressToResDto(address));
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Tuman topilmadi");
        }
    }

    @Override
    public ApiResult<CustomPage<AddressResDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "fullAddress");
        Page<Address> addressPage = addressRepository.findAll(pageable);
        CustomPage<AddressResDto> addressResDtoCustomPage = addressToCustomPage(addressPage);
        return ApiResult.successResponse(addressResDtoCustomPage);
    }

    @Override
    public ApiResult<AddressResDto> getOne(UUID id) {
        return ApiResult.successResponse(CustomMapper.addressToResDto(getAddressById(id)));
    }

    @Override
    public ApiResult<AddressResDto> edit(AddressReqDto addressReqDto, UUID id) {
        Address address = getAddressById(id);
        try {
            address.setDistrictId(addressReqDto.getDistrictId());
            address.setFullAddress(addressReqDto.getFullAddress());
            address.setLon(addressReqDto.getLon());
            address.setLat(addressReqDto.getLat());
            addressRepository.save(address);
            return ApiResult.successResponse(CustomMapper.addressToResDto(address));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "O'zgartirilgan addressning tumani bazadan topilmadi");
        }
    }


    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            addressRepository.deleteById(id);
            return ApiResult.successResponse("Muvafaqiyatli o'chirildi ");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "O'chirmoqchi bo'lgan address bazadan topilmadi");
        }
    }

    @Override
    public ApiResult<List<AddressForBranchDto>> getAddressesByIds(Set<UUID> ids) {
        List<Address> addresses = addressRepository.findAllById(ids);
        if (addresses.size() != ids.size())
            throw new RestException(HttpStatus.BAD_REQUEST,"Address lar to'liq topilmadi");

        List<AddressForBranchDto> addressForBranchDtos = addressMapper.addressToAddressForBranchDtoList(addresses);

        return ApiResult.successResponse(addressForBranchDtos);
    }

    @Override
    public ApiResult<AddressForBranchDto> getOneAddress(UUID id) {
        AddressForBranchDto address = addressMapper.addressToAddressForBranchDto(getAddressById(id));
        return ApiResult.successResponse(address);
    }


    private CustomPage<AddressResDto> addressToCustomPage(Page<Address> addressPage) {
        return new CustomPage<>(
                addressPage.getContent().stream().map(CustomMapper::addressToResDto).collect(Collectors.toList()),
                addressPage.getNumberOfElements(),
                addressPage.getNumber(),
                addressPage.getTotalElements(),
                addressPage.getTotalPages(),
                addressPage.getSize()
        );
    }
}
