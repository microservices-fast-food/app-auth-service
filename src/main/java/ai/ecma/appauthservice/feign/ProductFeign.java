package ai.ecma.appauthservice.feign;

import ai.ecma.appauthservice.config.FeignConfig;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.utils.RestConstant;
import ai.ecma.appdblib.entity.product.Attachment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Set;
import java.util.UUID;

@FeignClient(name = RestConstant.PRODUCT_SERVICE, configuration = FeignConfig.class)
public interface ProductFeign {
    String USER_BRANCH_CONROLLER = "/user-branch";

    @GetMapping("/api/open-product/attachment/info/{id}")
    ApiResult<Attachment> getById(@PathVariable(name = "id") UUID id);

    @GetMapping(USER_BRANCH_CONROLLER+"/get-all-courier-ids-by-branch/{branchId}")
    ApiResult<Set<UUID>> getAllCourierIdsByBranchId(@PathVariable(name = "branchId") UUID branchId);
}
