package ai.ecma.appauthservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DistrictReqDto {

    @NotBlank(message = "Tuman nomi bo'sh bo'lmasligi kerak")
    private String name;

    @NotNull(message = "Viloyat nomi bo'sh bo'lmasligi kerak")
    private UUID regionId;
}
