package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleResDto {
    private UUID id;
    private String name;
    private Set<PermissionEnum> permissions;
}
