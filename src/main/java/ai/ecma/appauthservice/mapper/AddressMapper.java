package ai.ecma.appauthservice.mapper;

import ai.ecma.appauthservice.payload.AddressForBranchDto;
import ai.ecma.appdblib.entity.user.Address;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    AddressForBranchDto addressToAddressForBranchDto(Address address);

    List<AddressForBranchDto> addressToAddressForBranchDtoList(List<Address> addresses);
}
