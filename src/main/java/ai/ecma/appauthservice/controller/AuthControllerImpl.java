package ai.ecma.appauthservice.controller;


import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.payload.*;
import ai.ecma.appauthservice.service.AuthService;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class AuthControllerImpl implements AuthController {

    private final AuthService authService;

    @Override
    public ApiResult<TokenDto> signIn(LoginDto loginDto) {
        return authService.signIn(loginDto);
    }

    @Override
    public ApiResult<?> checkPhoneNumber(@RequestBody @Valid PhoneNumberDto phoneNumberDto) {
        return authService.checkPhoneNumber(phoneNumberDto);
    }

    @Override
    public ApiResult<RegisterDto> checkCode(CodeDto codeDto) {
        return authService.checkCode(codeDto);
    }

    @Override
    public ApiResult<TokenDto> signUp(SignUpDto signUpDto) {
        return authService.signUp(signUpDto);
    }

    @Override
    public ApiResult<TokenDto> refreshToken(TokenDto tokenDto) {
        return authService.refreshToken(tokenDto);
    }

    @Override
    public ApiResult<User> checkUser() {
        return authService.checkUser();
    }

    @Override
    public ApiResult<User> checkPermission(PermissionEnum permissionEnum) {
        return authService.checkPermission(permissionEnum);
    }

    @Override
    public ApiResult<?> changePassword(ChangePasswordReqDto changePasswordReqDto, UserPrincipal userPrincipal) {
        return authService.changePassword(changePasswordReqDto, userPrincipal);
    }

    @Override
    public ApiResult<TokenDto> forgotPassword(ForgetPasswordReqDto forgetPasswordReqDto) {
        return authService.forgotPassword(forgetPasswordReqDto);
    }
}
