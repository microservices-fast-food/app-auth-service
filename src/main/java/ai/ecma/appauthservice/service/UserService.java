package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.payload.*;
import ai.ecma.appdblib.entity.enums.CourierStatusEnum;
import ai.ecma.appdblib.entity.user.User;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface UserService {
    ApiResult<UserResDto> add(UserReqDto userReqDto);

    ApiResult<CustomPage<UserResDto>> getAllByPageable(int page, int size);

    ApiResult<CustomPage<UserResDto>> getAllByRoleId(UUID roleId, int page, int size);

    ApiResult<UserResDto> getOne(UUID id);

    ApiResult<UserResDto> enableUser(UUID id, boolean enable);

    ApiResult<?> delete(UUID id);

    ApiResult<UserResDto> editUser(UUID id, UserEditReqDto userEditReqDto);

    ApiResult<List<UserDto>> getAllOnlineCouriersByBranch(UUID branchId);

    ApiResult<?> changeStatusCourier(CourierStatusEnum status, User user);

    ApiResult<List<UserDto>> getUsersByIds(Set<UUID> ids);
}
