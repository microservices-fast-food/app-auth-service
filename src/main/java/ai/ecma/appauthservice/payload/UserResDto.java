package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.LanguageEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResDto {
    private UUID id;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private DistrictResDto districtResDto;

    private Date birthdate;

    private UUID avatarId;

    private String avatarUrl;

    private RoleResDto roleResDto;  //bu roledto qaytarish kerak

    private boolean enabled;

    private LanguageEnum language;
}
