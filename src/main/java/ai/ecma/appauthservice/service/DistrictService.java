package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.DistrictReqDto;
import ai.ecma.appauthservice.payload.DistrictResDto;

import java.util.UUID;


public interface DistrictService {
    ApiResult<DistrictResDto> add(DistrictReqDto districtReqDto);

    ApiResult<CustomPage<DistrictResDto>> getAll(int page, int size);

    ApiResult<DistrictResDto> getOne(UUID id);

    ApiResult<DistrictResDto> edit(DistrictReqDto districtReqDto, UUID id);

    ApiResult<?> delete(UUID id);

}
