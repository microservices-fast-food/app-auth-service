package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.payload.*;
import ai.ecma.appauthservice.security.CurrentUser;
import ai.ecma.appauthservice.utils.RestConstant;
import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.User;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@RequestMapping(RestConstant.AUTH_CONTROLLER)
@Api(tags = "AUTH", description = "Tizimga kirish sozlamalari")
public interface AuthController {

    String SIGN_IN = "/sign-in";
    String CHECK_PHONE_NUMBER = "/check-phone-number";
    String CHECK_CODE = "/check-code";
    String SIGN_UP = "/sign-up";
    String REFRESH_TOKEN = "/refresh-token";
    String CHECK_USER = "/check-user";
    String CHECK_PERMISSION = "/check-permission";
    String CHANGE_PASSWORD = "/change-password";
    String FORGOT_PASSWORD = "/forgot-password";


    /**
     * Tizimga kirish
     *
     * @param loginDto (username, password)
     * @return TokenDto (accessToken , refreshToken)
     */
    @Operation(summary = "Tizimga kirish")
    @PostMapping(SIGN_IN)
    ApiResult<TokenDto> signIn(@RequestBody @Valid LoginDto loginDto);

    /**
     * Telefon raqamga sms code jonatish uchun
     *
     * @param phoneNumberDto (phonenumber)
     * @return "sms code yuborildi"
     */

    @Operation(summary = "Telefon raqamini tekshirish(Kod yuborish)")
    @PostMapping(CHECK_PHONE_NUMBER)
    ApiResult<?> checkPhoneNumber(@RequestBody @Valid PhoneNumberDto phoneNumberDto);


    /**
     * Telefonga kelgan sms codeni to'gri ekanligini tekshirib,confirmed = true qilib qoyadi
     * va tizimda oldin ro'yhatdan o'tgan bo'lgan bo'lsangiz tokenlar qaytaradi ,bo'lmasa null
     *
     * @param codeDto (phoneNumber,code)
     * @return RegisterDto(accessToken, refreshToken)
     */
    @Operation(summary = "Yuborilgan kod to'g'riligini tekshirish")
    @PostMapping(CHECK_CODE)
    ApiResult<RegisterDto> checkCode(@RequestBody @Valid CodeDto codeDto);


    /**
     * Yuborilgan codeni confirmmed=true bo'lganligini tekshirib, Ro'yhatga oladi
     *
     * @param signUpDto ()
     * @return TokenDto(accessToken, refreshToken)
     */
    @Operation(summary = "Tizimdan ro'yhatdan o'tish")
    @PostMapping(SIGN_UP)
    ApiResult<TokenDto> signUp(@RequestBody @Valid SignUpDto signUpDto);


    /**
     * Access token muddati o'tgan bo'lsa refresh token yordamida yangilash
     *
     * @param tokenDto (accessToken,refreshToken)
     * @return TokenDto(accessToken, refreshToken)
     */
    @Operation(summary = "Tokenni yangilash")
    @PostMapping(REFRESH_TOKEN)
    ApiResult<TokenDto> refreshToken(@RequestBody TokenDto tokenDto);


    /**
     * Auvtorizatsiyadan o'tgan user ekanligini bilish
     *
     * @return User
     */
    @Operation(summary = "Foydalanuvchini tekshirish")
    @GetMapping(CHECK_USER)
    ApiResult<User> checkUser();


    /**
     * Userning huquqi bor ekanligini tekshirish
     *
     * @param permissionEnum (enum)
     * @return User
     */
    @Operation(summary = "Huquqlarni tekshirish")
    @PostMapping(CHECK_PERMISSION)
    ApiResult<User> checkPermission(@RequestBody PermissionEnum permissionEnum);

    /**
     * Eski passwordini yangi passwordga o'zgartirish
     *
     * @param changePasswordReqDto (oldPassword, newPassword, confirmPassword)
     * @param userPrincipal
     * @return "Muvaffaqiyatli o'zgartirilganli haqida xabar"
     */
    @Operation(summary = "Parolni o'zgartirish")
    @PostMapping(CHANGE_PASSWORD)
    ApiResult<?> changePassword(@RequestBody @Valid ChangePasswordReqDto changePasswordReqDto,
                                @ApiIgnore @CurrentUser UserPrincipal userPrincipal);


    /**
     * Passwordini esdan chiqarib qo'yganda eski parolni bilmasdan turib yangilash
     *
     * @param forgetPasswordReqDto (phoneNumber,code,newPassword)
     * @return TokenDto(accessToken, refreshToken)
     */
    @Operation(summary = "Parolni tiklash")
    @PostMapping(FORGOT_PASSWORD)
    ApiResult<TokenDto> forgotPassword(@RequestBody @Valid ForgetPasswordReqDto forgetPasswordReqDto);

}
