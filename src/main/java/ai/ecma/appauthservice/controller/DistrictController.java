package ai.ecma.appauthservice.controller;

import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.CustomPage;
import ai.ecma.appauthservice.payload.DistrictReqDto;
import ai.ecma.appauthservice.payload.DistrictResDto;
import ai.ecma.appauthservice.utils.RestConstant;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

import static ai.ecma.appauthservice.utils.AppConstant.DEFAULT_PAGE_NUMBER;
import static ai.ecma.appauthservice.utils.AppConstant.DEFAULT_PAGE_SIZE;

@RequestMapping(RestConstant.DISTRICT_CONTROLLER)
public interface DistrictController {

    @PostMapping
    ApiResult<DistrictResDto> add(@RequestBody @Valid DistrictReqDto districtReqDto);

    @GetMapping
    ApiResult<CustomPage<DistrictResDto>> getAll(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
                                                 @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size);

    @GetMapping("/{id}")
    ApiResult<DistrictResDto>getOne(@PathVariable UUID id);

    @PutMapping("/{id}")
    ApiResult<DistrictResDto> edit(@RequestBody @Valid DistrictReqDto districtReqDto, @PathVariable UUID id);

    @DeleteMapping("/{id}")
    ApiResult<?>delete(@PathVariable UUID id);

}
