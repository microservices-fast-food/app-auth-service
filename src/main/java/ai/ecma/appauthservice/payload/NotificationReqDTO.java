package ai.ecma.appauthservice.payload;

import ai.ecma.appdblib.entity.enums.ReceiverTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationReqDTO {


    @NotBlank(message = "Title bo'sh bo'lmasligi kerak")
    private String title;

    @NotBlank(message = "Text bo'sh bo'lmasligi kerak")
    private String text;

    private UUID photoId;

    private Timestamp sendTime;

    @NotNull(message = "Qabul qilib oluvchining typi bo'sh bo'lmasligi kerak")
    private ReceiverTypeEnum receiver;
}
