package ai.ecma.appauthservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAddressReqDto {

//    @NotNull(message = "User bo'sh bo'lmasligi kerak")
//    private UUID userId;

    @NotNull(message = "Address bo'sh bo'lmasligi kerak")
    private UUID addressId;

    private String name;

}
