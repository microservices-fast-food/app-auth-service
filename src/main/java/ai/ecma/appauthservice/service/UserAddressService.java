package ai.ecma.appauthservice.service;

import ai.ecma.appauthservice.common.UserPrincipal;
import ai.ecma.appauthservice.payload.ApiResult;
import ai.ecma.appauthservice.payload.UserAddressReqDto;
import ai.ecma.appauthservice.payload.UserAddressResDto;
import ai.ecma.appdblib.payload.SpecialAddressResDto;

import java.util.List;
import java.util.UUID;

public interface UserAddressService {

    ApiResult<UserAddressResDto> add(UserAddressReqDto userAddressReqDto, UserPrincipal userPrincipal);

    ApiResult<List<SpecialAddressResDto>> getAllByUserId(UUID userId);

    ApiResult<UserAddressResDto> editName(UserAddressReqDto userAddressReqDto, UserPrincipal userPrincipal);

    ApiResult<?> deleteAddress(UserAddressReqDto userAddressReqDto);

}
