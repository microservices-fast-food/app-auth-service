package ai.ecma.appauthservice.mapper;

import ai.ecma.appauthservice.payload.UserDto;
import ai.ecma.appdblib.entity.user.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsersMapper {

    UserDto userToCourierDto(User user);

    List<UserDto> usersToUserDtoList(List<User> userList);
}
